<?php

namespace App\Entity;

use App\Repository\ToDoListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ToDoListRepository::class)]
class ToDoList
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'toDoList', targetEntity: ToDoItem::class)]
    private Collection $toDoItems;

    #[ORM\ManyToOne(inversedBy: 'lists')]
    private ?User $user = null;

    public function __construct()
    {
        $this->toDoItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @return Collection<int, ToDoItem>
     */
    public function getToDoItems(): Collection
    {
        return $this->toDoItems;
    }

    public function addToDoItem(ToDoItem $toDoItem): self
    {
        if (!$this->toDoItems->contains($toDoItem)) {
            $this->toDoItems->add($toDoItem);
            $toDoItem->setToDoList($this);
        }

        return $this;
    }

    public function removeToDoItem(ToDoItem $toDoItem): self
    {
        if ($this->toDoItems->removeElement($toDoItem)) {
            // set the owning side to null (unless already changed)
            if ($toDoItem->getToDoList() === $this) {
                $toDoItem->setToDoList(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
