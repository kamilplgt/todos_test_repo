<?php

namespace App\Controller;

use App\Entity\ToDoList;
use App\Form\ToDoListFormType;
use App\Repository\ToDoListRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class ToDoListController extends AbstractController
{
    #[Route('/', name: 'app_to_do_list', methods: ['GET', 'POST'])]
    public function index(
        Request                $request,
        EntityManagerInterface $entityManager,
        UserInterface          $user,
        ToDoListRepository     $toDoListRepository,
    ): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $toDoList = new ToDoList();
        $form = $this->createForm(ToDoListFormType::class, $toDoList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $toDoList = $form->getData();
            $user->addList($toDoList);
            $entityManager->persist($toDoList);
            $entityManager->flush();

            return $this->redirectToRoute('app_to_do_list');
        }

        $lists = $toDoListRepository->findAll();

        return $this->render('to_do_list/toDoList.html.twig', [
            'form' => $form->createView(),
            'lists' => $lists
        ]);
    }
}
