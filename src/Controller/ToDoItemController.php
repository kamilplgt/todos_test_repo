<?php

namespace App\Controller;

use App\Entity\ToDoItem;
use App\Form\ToDoItemFormType;
use App\Repository\ToDoItemRepository;
use App\Repository\ToDoListRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ToDoItemController extends AbstractController
{
    #[Route('/list/{id}', name: 'app_to_do_item')]
    public function index(
        Request                $request,
        EntityManagerInterface $entityManager,
        ToDoItemRepository     $toDoItemRepository,
        ToDoListRepository     $toDoListRepository,
        string                 $id
    ): Response
    {

        $this->denyAccessUnlessGranted('ROLE_USER');

        $toDoItem = new ToDoItem();
        $form = $this->createForm(ToDoItemFormType::class, $toDoItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $toDoItem = $form->getData();
            $toDoItem->setIsCompleted(false);
            $toDoList = $toDoListRepository->findOneBy(['id' => $id]);


            $toDoList->addToDoItem($toDoItem);
            $entityManager->persist($toDoItem);
            $entityManager->flush();


            return $this->redirect($request->getRequestUri());
        }

        $id = intval($id);
        $items = $toDoItemRepository->findAll();
        $items = Array_filter($items, fn($item) => $item->getToDoList()->getId() === $id);


        return $this->render('to_do_item/toDoItem.html.twig', [
            'form' => $form->createView(),
            'items' => $items,
            'id' => $id,
        ]);
    }
}
