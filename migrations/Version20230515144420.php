<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230515144420 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE to_do_item (id INT AUTO_INCREMENT NOT NULL, to_do_list_id INT DEFAULT NULL, list_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, is_completed TINYINT(1) NOT NULL, INDEX IDX_11B395EAB3AB48EB (to_do_list_id), INDEX IDX_11B395EA3DAE168B (list_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE to_do_list (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_4A6048ECA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE to_do_item ADD CONSTRAINT FK_11B395EAB3AB48EB FOREIGN KEY (to_do_list_id) REFERENCES to_do_list (id)');
        $this->addSql('ALTER TABLE to_do_item ADD CONSTRAINT FK_11B395EA3DAE168B FOREIGN KEY (list_id) REFERENCES to_do_list (id)');
        $this->addSql('ALTER TABLE to_do_list ADD CONSTRAINT FK_4A6048ECA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE to_do_item DROP FOREIGN KEY FK_11B395EAB3AB48EB');
        $this->addSql('ALTER TABLE to_do_item DROP FOREIGN KEY FK_11B395EA3DAE168B');
        $this->addSql('ALTER TABLE to_do_list DROP FOREIGN KEY FK_4A6048ECA76ED395');
        $this->addSql('DROP TABLE to_do_item');
        $this->addSql('DROP TABLE to_do_list');
    }
}
