<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230515210318 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE to_do_item_to_do_list (to_do_item_id INT NOT NULL, to_do_list_id INT NOT NULL, INDEX IDX_ACBCDF79C6A0C3E (to_do_item_id), INDEX IDX_ACBCDF7B3AB48EB (to_do_list_id), PRIMARY KEY(to_do_item_id, to_do_list_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE to_do_item_to_do_list ADD CONSTRAINT FK_ACBCDF79C6A0C3E FOREIGN KEY (to_do_item_id) REFERENCES to_do_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE to_do_item_to_do_list ADD CONSTRAINT FK_ACBCDF7B3AB48EB FOREIGN KEY (to_do_list_id) REFERENCES to_do_list (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE to_do_item DROP FOREIGN KEY FK_11B395EA3DAE168B');
        $this->addSql('DROP INDEX IDX_11B395EA3DAE168B ON to_do_item');
        $this->addSql('ALTER TABLE to_do_item DROP list_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE to_do_item_to_do_list DROP FOREIGN KEY FK_ACBCDF79C6A0C3E');
        $this->addSql('ALTER TABLE to_do_item_to_do_list DROP FOREIGN KEY FK_ACBCDF7B3AB48EB');
        $this->addSql('DROP TABLE to_do_item_to_do_list');
        $this->addSql('ALTER TABLE to_do_item ADD list_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE to_do_item ADD CONSTRAINT FK_11B395EA3DAE168B FOREIGN KEY (list_id) REFERENCES to_do_list (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_11B395EA3DAE168B ON to_do_item (list_id)');
    }
}
