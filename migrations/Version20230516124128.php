<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230516124128 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE to_do_item ADD list_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE to_do_item ADD CONSTRAINT FK_11B395EA3DAE168B FOREIGN KEY (list_id) REFERENCES to_do_list (id)');
        $this->addSql('CREATE INDEX IDX_11B395EA3DAE168B ON to_do_item (list_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE to_do_item DROP FOREIGN KEY FK_11B395EA3DAE168B');
        $this->addSql('DROP INDEX IDX_11B395EA3DAE168B ON to_do_item');
        $this->addSql('ALTER TABLE to_do_item DROP list_id');
    }
}
